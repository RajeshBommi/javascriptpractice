//package ZPractice;
//
//import java.util.Scanner;
//
//public class MatrixAddition {
//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        int n = scanner.nextInt(); // Number of arrays
//        scanner.nextLine(); // Consume newline
//
//        long sum = 0; // Initialize the sum
//
//        // Iterate through each array
//        for (int i = 0; i < n; i++) {
//            String[] arrayStr = scanner.nextLine().split(" "); // Read array as string
//            sum += arrayToNumber(arrayStr); // Convert array to number and add to sum
//        }
//
//        System.out.println(sum); // Output the sum
//    }
//
//    // Method to convert array to number
//    public static long arrayToNumber(String[] arrayStr) {
//        long num = 0;
//        for (String str : arrayStr) {
//            num = num * 10 + Integer.parseInt(str); // Convert string to integer and add to number
//        }
//        return num;
//    }
//}
//

package ZPractice;

import java.util.Scanner;

public class MatrixAddition {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt(); // Number of arrays
        scanner.nextLine(); // Consume newline

        long sum = 0; // Initialize the sum

        // Iterate through each array
        for (int i = 0; i < n; i++) {
            String[] arrayStr = scanner.nextLine().split(" "); // Read array as string
            if (arrayStr.length == 0) {
                continue; // Skip empty lines
            }
            sum += arrayToNumber(arrayStr); // Convert array to number and add to sum
        }

        System.out.println(sum); // Output the sum
    }

    // Method to convert array to number
    public static long arrayToNumber(String[] arrayStr) {
        long num = 0;
        for (String str : arrayStr) {
            if (!str.isEmpty()) { // Check if string is not empty
                num = num * 10 + Integer.parseInt(str); // Convert string to integer and add to number
            }
        }
        return num;
    }
}

