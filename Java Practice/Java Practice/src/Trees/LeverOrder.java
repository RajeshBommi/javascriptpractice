package Trees;

import javax.swing.tree.TreeNode;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class LeverOrder {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the root value");
        int root = sc.nextInt();
        CreateNode rootValue = new CreateNode(root);

        addChild(rootValue, sc);

        System.out.println("Level Traversal");

        leverOrder(rootValue);

    }
    public static void addChild(CreateNode node , Scanner sc) {
        System.out.println("Do you want to enter the left child for root value" + node.val + "Y/N??");
        char choice = sc.next().charAt(0);
        if (choice == 'Y') {
            System.out.println("Enter the left child value");
            int left = sc.nextInt();
            node.left = new CreateNode(left);
            addChild(node.left, sc);
        }
        System.out.println("Do you want to enter the right child for root value" + node.val + "Y/N??");
        choice = sc.next().charAt(0);
        if (choice == 'Y') {
            System.out.println("Enter the right child value");
            int right = sc.nextInt();
            node.right = new CreateNode(right);
            addChild(node.right, sc);
        }
    }




    public  static  void leverOrder(CreateNode root){
        Queue<CreateNode>  treeQueue = new LinkedList<>();
        treeQueue.add(root);
        while (!treeQueue.isEmpty()){
            CreateNode node=treeQueue.poll();
            System.out.println(node.val+"->");

            if(node.left!=null){
                treeQueue.add(node.left);
            }
            if(node.right!=null){
                treeQueue.add(node.right);
            }
        }
    }
}
