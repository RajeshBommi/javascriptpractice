package Trees;

public class Node {
        int data;
        Node left, right;

        public Node(int data) {
            this.data = data;
            this.left = this.right = null;
        }




    static int sum = 0;
        public static void  main(String[] args) {
            /* Construct the tree as given in the example */
            Node root = new Node(1);
            root.left = new Node(2);
            root.right = new Node(3);
            root.left.left = new Node(4);
            root.left.right = null;
            root.right.right = new Node(5);
            root.left.left.left = new Node(6);
            root.left.left.right = new Node(7);
            root.right.right.right = new Node(8);


            int result = sumOfSingleChildNodes(root);
            System.out.println("Sum of values of nodes having only one child: " + result);
        }

        public static int sumOfSingleChildNodes(Node root){

            if(root==null){
                return 0;
            }

            if((root.left!=null && root.right==null)|| (root.left==null && root.right!=null)){
                sum+=root.data;
            }
            sumOfSingleChildNodes(root.left);
            sumOfSingleChildNodes(root.right);
            return sum;
        }

}
