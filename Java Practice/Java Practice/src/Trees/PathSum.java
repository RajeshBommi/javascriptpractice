package Trees;

import java.util.Scanner;

public class PathSum {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the root value");
        int root = sc.nextInt();
        CreateNode rootValue = new CreateNode(root);

        addChild(rootValue, sc);

        System.out.println("sum of the path:"+  hasPath(rootValue,4));
        System.out.println("Tree has path: " + hasPath(rootValue, 23));
        System.out.println("Tree has path: " + hasPath(rootValue, 16));


    }
    public static void addChild(CreateNode node , Scanner sc) {
        System.out.println("Do you want to enter the left child for root value" + node.val + "Y/N??");
        char choice = sc.next().charAt(0);
        if (choice == 'Y') {
            System.out.println("Enter the left child value");
            int left = sc.nextInt();
            node.left = new CreateNode(left);
            addChild(node.left, sc);
        }
        System.out.println("Do you want to enter the right child for root value" + node.val + "Y/N??");
        choice = sc.next().charAt(0);
        if (choice == 'Y') {
            System.out.println("Enter the right child value");
            int right = sc.nextInt();
            node.right = new CreateNode(right);
            addChild(node.right, sc);
        }
    }

    public static boolean hasPath(CreateNode root,int sum){
        if(root==null){
            return false;
        }
        if(root.val==sum && root.left==null && root.right==null){
            return true;
        }

        return hasPath(root.left,sum-root.val) || hasPath(root.right,sum- root.val);
    }

}
