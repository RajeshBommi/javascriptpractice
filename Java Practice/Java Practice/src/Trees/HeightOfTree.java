package Trees;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class HeightOfTree {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the root value");
        int root = sc.nextInt();
        CreateNode rootValue = new CreateNode(root);

        addChild(rootValue, sc);
        System.out.println("InOrder Traversal");
        System.out.println("The height of tree is:"+height(rootValue));

    }
    public static void addChild(CreateNode node , Scanner sc) {
        System.out.println("Do you want to enter the left child for root value" + node.val + "Y/N??");
        char choice = sc.next().charAt(0);
        if (choice == 'Y') {
            System.out.println("Enter the left child value");
            int left = sc.nextInt();
            node.left = new CreateNode(left);
            addChild(node.left, sc);
        }
        System.out.println("Do you want to enter the right child for root value" + node.val + "Y/N??");
        choice = sc.next().charAt(0);
        if (choice == 'Y') {
            System.out.println("Enter the right child value");
            int right = sc.nextInt();
            node.right = new CreateNode(right);
            addChild(node.right, sc);
        }
    }

    public static int height(CreateNode root){
        Queue<CreateNode> eleQue= new LinkedList<>();
        eleQue.add(root);
        int numOfLevels=-1;

        while (true){
            int nodesAtLevels= eleQue.size();  //Time Complexity and spaceComplexity o(n)
            if(nodesAtLevels==0){
                return numOfLevels;
            }
            while (nodesAtLevels>0){
                CreateNode ele=eleQue.poll();
//                assert ele != null;
                if(ele.left!=null){
                    eleQue.add(ele.left);
                }
                if(ele.right!=null){
                    eleQue.add(ele.right);

                }
                nodesAtLevels--;

            }
            numOfLevels++;

        }

    }
}
