package Strings;

public class LengthOfLastWord {

    public static void main(String[] args) {
        String str="Hello world ";
        System.out.println(length(str));
    }

    public static int length(String sentence){
        sentence=sentence.trim();
        int end = sentence.length()-1;
        int start = end;

        while(end>=0 && sentence.charAt(end)==' '){
            end--;
        }
        while(start>=0 && sentence.charAt(start)!=' '){
            start--;
        }

        return end-start;
    }
}
