package Hashing;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class RepeatedK {

    public static void main(String[] args) {
        int[] arr ={1,2,2,3,4,5,2,6,7,4,4,4};
        int k=2;
        List<Integer> l=repeat(arr);
        System.out.println(l);
    }

//    public static List<Integer> repeat(int[] arr){
//        List<Integer> list = new ArrayList<>();
//        Map<Integer,Integer> map = new HashMap<>();
//
//        for(int arr1 : arr){
//            map.put(arr1,map.getOrDefault(arr1,0)+1);
//        }
//        int max= Collections.max(map.values());
//
//        for(Map.Entry<Integer,Integer> entry : map.entrySet()){
//            if(entry.getValue()==max){
//                list.add(entry.getKey());
//            }
//        }
//        return list;
//    }

    public static List<Integer> repeat(int [] arr){

        List<Integer> l = new ArrayList<>();
        HashMap<Integer,Integer> map = new HashMap<>();

        for(int a:arr){
            map.put(a,map.getOrDefault(a,0)+1);
        }

        int max = Collections.max(map.values());

        for(Map.Entry<Integer,Integer> m : map.entrySet()){
            if(m.getValue()==max){
                l.add(m.getKey());
            }
        }
        return l;
    }
}
