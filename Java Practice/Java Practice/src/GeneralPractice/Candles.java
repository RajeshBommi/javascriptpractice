package GeneralPractice;

public class Candles {

    public static void main(String[] args) {
        int n=12;
        int k=4;
        Candles c= new Candles();
        int a=c.candles(n,k);
        System.out.println("Total number of Candles:"+ a);
    }

    public int candles(int n,int k){
        int sum=n;

        while(n>=k){
            int newCandles=n/k;
            sum=sum+newCandles;
            n=n%k+newCandles;
        }
        return sum;
    }
}
