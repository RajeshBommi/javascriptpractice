package Arrays;

import java.util.Arrays;

public class RemoveDuplicates {

    public static void main(String[] args) {
        int[] arr = {1, 1, 2, 3, 3, 4, 5};
        int[] k=removeDup(arr);
        System.out.println(Arrays.toString(k));
    }
    public static int[] removeDup(int[] arr){
        int j=0;
        for(int i = 0; i< arr.length-1; i++){
            if(arr[i]!=arr[i+1]){
                arr[j++]=arr[i];
            }
        }
        arr[j+1]=arr[arr.length-1];
        int[] result = new int[j];
        for(int i=0;i<j;i++){
            result[i]=arr[i];
        }
        return result;
    }
}
