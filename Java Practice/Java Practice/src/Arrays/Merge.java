package Arrays;


import java.util.*;

class Merge {

    public void merge(int[] nums1 ,int[] nums2) {

//        int[] mergeArray = new int[nums1.length+nums2.length];
        List<Integer> mergeList= new ArrayList<>();
        int p=0;
        for(int ele:nums1){
            mergeList.add(ele);
//            p++;
        }
        for(int ele:nums2){
            mergeList.add(ele);
//            p++;
        }
        mergeList.removeIf(num->num==0);
        Collections.sort(mergeList);

        int [] mergeArray= new int[mergeList.size()];
        for(int i=0;i<mergeList.size();i++){
            mergeArray[i]=mergeList.get(i);
        }
        System.out.println(Arrays.toString(mergeArray));


    }
    public static void main(String[]args){
        Merge m= new Merge();
        int[] nums = {1,2,3,0,0,0};
        int[]  nums3 = {2,5,6};
        m.merge(nums,nums3);


    }
}
