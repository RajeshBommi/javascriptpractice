//package Arrays;
//
////public class Capital {
////
////    public static void main(String[] args) {
////
////        String input = "java is a programming language";
////        StringBuilder str = new StringBuilder();
////
//
////        String[] words = input.split(" ");
////        for(String word : words){
////            str.append(Character.toUpperCase(word.charAt(0)));
////            if(word.length()>1){
////                str.append(word.substring(1));
////            }
////            str.append(" ");
////        }
////        System.out.println(str.toString().trim());
////    }
////}
//
////import java.util.Arrays;
////
////public class Capital {
////    public static void main(String[] args) {
////        int[] array = {5, 3, 8, 1, 9, 2};
////        Arrays.sort(array);
////        System.out.println(Arrays.toString(array));
////    }
////}
//
//import java.util.LinkedHashMap;
//import java.util.Map;
//
//public class Capital {
//    public static Character firstNonRepeatedCharacter(String s) {
//        Map<Character, Integer> charCount = new LinkedHashMap<>();
//        for (char c : s.toCharArray()) {
//            charCount.put(c, charCount.getOrDefault(c, 0) + 1);
//        }
//        for (Map.Entry<Character, Integer> entry : charCount.entrySet()) {
//            if (entry.getValue() == 1) {
//                return entry.getKey();
//            }
//        }
//        return null;
//    }
//
//    public static void main(String[] args) {
//        String str = "Lollipop";
//        System.out.println(firstNonRepeatedCharacter(str.toLowerCase()));  // Output: 'w'
//    }
//}
//
//import java.util.stream.IntStream;
//
//public class OnlyDigits {
//    public static boolean containsOnlyDigits(String s) {
//        return s.chars().allMatch(Character::isDigit);
//    }
//
//    public static void main(String[] args) {
//        String str = "12345";
//        System.out.println(containsOnlyDigits(str));  // Output: true
//        str = "123a5";
//        System.out.println(containsOnlyDigits(str));  // Output: false
//    }
//}
//
//
