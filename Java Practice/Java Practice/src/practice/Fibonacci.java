//package practice;
//
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Scanner;
//import java.util.Stack;
//
//public class Fibonacci {
//
//    private static final Map<Integer,Integer> map= new HashMap<>();
//
//    public static void main(String[] args) {
//        Scanner sc= new Scanner(System.in);
//        System.out.println("Enter a number");
//        int a=sc.nextInt();
//        System.out.println(fibonacci(a));
//    }
//
//
//
//    public static  int fibonacci(int n){
//        if(n==0) return 0;
//
//        if(n==1) return 1;
//
//        if(map.containsKey(n)){
//            return map.get(n);
//        }else{
//            int fib=fibonacci(n-1)+fibonacci(n-2);
//            map.put(n,fib);
//            return fib;
//        }
//    }
//}
package practice;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Fibonacci {

    private static final Map<Integer, Integer> map = new HashMap<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a number");
        int n = sc.nextInt();
        for (int i = 0; i < n; i++) {
            System.out.print(fibonacci(i) + " ");
        }
        System.out.println(fibonacci(n));
    }

    public static int fibonacci(int n) {
        // Base cases
        if (n == 0) return 0;
        if (n == 1) return 1;

        // Check if fibonacci(n) has already been computed
        if (map.containsKey(n)) {
            return map.get(n);
        } else {
            // Compute fibonacci(n) using recursion and memoization
            int fib = fibonacci(n - 1) + fibonacci(n - 2);
            map.put(n, fib);
            return fib;
        }
    }

//    public  static int fibonacci(int n){
//        if(n==0)
//            return 0;
//
//        if(n==1)
//            return 1;
//
//        if(map.containsKey(n)){
//            return map.get(n);
//        }
//        else{
//            int fib=fibonacci(n-1)+fibonacci(n-2);
//            map.put(n,fib);
//            return fib;
//        }
//    }
}

