package practice;

import java.util.Stack;

public class ReversePolish {

    public static void main(String[] args) {

        String[] tokens = {"2","1","+","3","*"};
        System.out.println(calculate(tokens));

    }

    public static int calculate(String [] tokens){

        Stack<Integer> s= new Stack<>();
        int v1;
        int v2;
        for (String token : tokens) {
            switch (token) {
                case "+":
                    s.push(s.pop() + s.pop());
                    break;
                case "-":
                    v1 = s.pop();
                    v2 = s.pop();
                    s.push(v1 - v2);
                    break;
                case "*":
                    s.push(s.pop() * s.pop());
                    break;
                case "/":
                    v1 = s.pop();
                    v2 = s.pop();
                    s.push(v1 / v2);
                    break;
                default:
                    s.push(Integer.parseInt(token));

            }
        }
        return s.pop();
    }
}
