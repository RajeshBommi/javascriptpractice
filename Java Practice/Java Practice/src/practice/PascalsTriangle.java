package practice;

import java.util.Scanner;

public class PascalsTriangle {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a number");
        int a=sc.nextInt();
        pascal(a);
    }

    public static void pascal(int k){
        for(int i=1;i<=k;i++){
            for(int b=0;b<=k-i;b++){
                System.out.print(" ");
            }
            int c=1;
            for(int a=1;a<=i;a++){
                System.out.print(c+" ");
                c=c*(i-a)/a;
            }
            System.out.println( );
        }
    }
}
