package practice;

import java.util.Scanner;

public class SimpleInterest {

    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);
        System.out.println("Enter the Principal amount");
        int p=sc.nextInt();
        System.out.println("Enter the time ");
        int t=sc.nextInt();
        System.out.println("Enter the period ");
        int r=sc.nextInt();
        interest(p,t,r);
    }

    public static void interest(int a,int b,int c){
        System.out.println("The simple interest is "+((a*b*c)/100));
    }
}
