package Hashing;

import java.util.HashMap;

public class PalindromeCheck {

    public static void main(String[] args) {
        System.out.println(palindrome("Madam"));
        System.out.println(palindrome("World"));

    }

//    public static boolean palindrome(String str){
//        str=str.toLowerCase().replaceAll("[^a-z0-9]", "");
//        HashMap<Character,Integer> map = new HashMap<>();
//
//        for(char c : str.toCharArray()){
//            map.put(c,map.getOrDefault(c,0)+1);
//        }
//
//        int oddCount=0;
//        for(int i : map.values()){
//            if(i%2!=0){
//                oddCount++;
//            }
//            if(oddCount>1){
//                return false;
//            }
//        }
//        return true;
//    }

    public static boolean palindrome (String str){
        str=str.toLowerCase().replaceAll("[^a-z0-9]","");

        HashMap<Character,Integer> map = new HashMap<>();

        for(char c : str.toCharArray()){
            map.put(c,map.getOrDefault(c,0)+1);
        }

        int oddCount=0;
        for(int i:map.values()){
            if(i%2!=0){
                oddCount++;
            }
            if(oddCount>1){
                return  false;
            }
        }
        return true;
    }
}
