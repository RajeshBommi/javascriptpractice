package ZPractice;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class ChangeKeyToUc {

    public static void main(String[] args) {

        Map<String, String> map = new HashMap<>();
        map.put("Key1", "Value1");
        map.put("Key2", "Value2");
        map.put("Key3", "Value3");

        Map<String, String> newMap = new HashMap<>();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String uc = entry.getKey().toUpperCase();
            String val = entry.getValue();
//            newMap.put(uc,val);
            System.out.println(uc + ":" + val);
//        }
//
//        System.out.println("After uppercase");
//        for(Map.Entry<String,String > entry : newMap.entrySet()){
//            System.out.println(entry.getKey()+ ":" + entry.getValue());
//        }

//        Map<String,String>  newMap= map.entrySet().stream().collect(Collectors.toMap(entry-> entry.getKey().toUpperCase(),Map.Entry::getValue));
//        System.out.println("uppercase keys");
//        newMap.forEach((key,value)->System.out.println(key+ ":"+value));


        }

    }
}
