package ZPractice;

import java.util.HashMap;

public class Pangram {

    public static void main(String[] args) {
//        int [] arr= new int[26];
        System.out.println(pangram("abc defGhi JklmnOP QRStuv wxyzz"));

    }

    public static boolean pangram(String s){

        HashMap<Character,Integer> map=new HashMap<>();

        for(char c : s.toCharArray()){
            if(Character.isLetter(c)){
                map.put(c,map.getOrDefault(c,0)+1);
            }
        }
        return  map.size()==26;
    }

}
