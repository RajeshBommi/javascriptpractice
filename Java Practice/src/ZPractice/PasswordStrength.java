package ZPractice;

public class PasswordStrength {

    public static void main(String[] args) {
        System.out.println(pStrength("Qw!1 "));
        System.out.println(pStrength("Qwertyuiop "));
        System.out.println(pStrength(" QwertY123   "));
        System.out.println(pStrength(" Qwerty@123"));
    }

    public static String pStrength(String s){

        if(s.length()<8){
            return "weak";
        }

        boolean upper=false;
        boolean lower=false;
        boolean number=false;
        boolean specialChar=false;


        for(char c:s.toCharArray()){
            if(Character.isUpperCase(c)){
                upper=true;
            }
            if(Character.isLowerCase(c)){
                lower=true;
            }
            if(Character.isDigit(c)){
                number=true;
            }
            else{
                specialChar=true;
            }
        }
        if(upper && lower && number && specialChar){
            return "Strong";
        } else if (upper && lower ||
                   upper && number||
                   upper && specialChar||
                   lower && number||
                   lower && specialChar||
                    number && specialChar) {
            return "Good";

        }
        else if (upper || lower || number || specialChar){
            return "Medium";
        }else{
            return  "Weak";
        }

    }
}
