package ZPractice;

//public class CricketScore {
//
//    public static void main(String[] args) {
//        String[] s = {"1", ".", "2", ".", "4", "3", "6", "W", "1", ".", "N", ".", "2", "1"};
//        int[] individualScore = score(s);
//        System.out.println("P1 - " + individualScore[0]);
//        System.out.println("P2 - " + individualScore[1]);
//        System.out.println("Extras - " + individualScore[2]);
//    }
//
//    public static int[] score(String[] score) {
//        int player1 = 0;
//        int player2 = 0;
//        int extras = 0;
//        boolean player1Turn = true;
//
//        for (String str : score) {
//            if (str.equals("W") || str.equals("N")) {
//                extras++;
//            } else if (!str.equals(".")) {
//                int runs = Integer.parseInt(str);
////                String currentPlayer = player1Turn ? "P1" : "P2";
//                if (player1Turn) {
//                    player1 += runs;
//                } else {
//                    player2 += runs;
//                }
//                // Toggle player turn after each valid score
//                if (str.equals("1")||str.equals("3")) {
//                    player1Turn = !player1Turn;
//                }
//            }
//        }
//        return new int[]{player1, player2, extras};
//    }
//}

import java.util.*;

public class CricketScore {

    public static void main(String[] args) {
        String[] timeline = {"1", ".", "2", ".", "4", "3", "6", "W", "1", ".", "N", ".", "2", "1"};
        Map<String, Integer> result = calculateScores(timeline);
        System.out.println("P1 - " + result.getOrDefault("P1", 0));
        System.out.println("P2 - " + result.getOrDefault("P2", 0));
        System.out.println("Extras - " + result.getOrDefault("Extras", 0));
    }

    public static Map<String, Integer> calculateScores(String[] timeline) {
        Map<String, Integer> scores = new HashMap<>();
        scores.put("P1", 0);
        scores.put("P2", 0);
        scores.put("Extras", 0);
        boolean player1Turn = true; // Indicates whose turn it is to bat

        for (String score : timeline) {
            if (score.equals("W") || score.equals("N")) {
                scores.put("Extras", scores.get("Extras") + 1);
            } else if (!score.equals(".")) {
                int runs = Integer.parseInt(score);
                String currentPlayer = player1Turn ? "P1" : "P2";
                scores.put(currentPlayer, scores.get(currentPlayer) + runs);
                if(score.equals("1")||score.equals("3")) {
                    player1Turn = !player1Turn;
                }// Switch turns after each score
            }
        }

        return scores;
    }
}


