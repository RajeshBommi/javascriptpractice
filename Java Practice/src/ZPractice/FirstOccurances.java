package ZPractice;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class FirstOccurances {
    public static void main(String[] args) {
          System.out.println(fc("ZOHOCORPORATION ","PORT"));
    }

    public static String  fc(String s1,String s2){
        HashMap<Character,Integer> map= new HashMap<>();
        for(int i=0;i<s1.length();i++){
            char c= s1.charAt(i);
            if(s2.indexOf(c)!=-1 && !map.containsKey(c)){
                map.put(c,i);
            }
        }
        int min=Integer.MAX_VALUE;
        int max=Integer.MIN_VALUE;
        for(char c:s2.toCharArray()){
            if(map.containsKey(c)){
                min= Math.min(min,map.get(c));
                max=Math.max(max,map.get(c));
            }
        }

        if(min!=Integer.MAX_VALUE && max!=Integer.MIN_VALUE){
            return s1.substring(min,max+1);
        }else {
            return "Not in range";
        }
    }
}
