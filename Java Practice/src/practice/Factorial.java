package practice;

import java.util.Scanner;

public class Factorial {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a number");
        int a=sc.nextInt();
        int result = fac(a);
        System.out.println(result);
    }

    public static int fac(int n){
        if(n==0){
            return 1;
        }
        return n* fac(n-1);
    }
}
