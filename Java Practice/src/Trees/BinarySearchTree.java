package Trees;

import java.util.Scanner;

public class BinarySearchTree {

    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        System.out.println("Enter the root value of tree");
        int root=sc.nextInt();
        CreateNode rootValue= new CreateNode(root);
        addChild(rootValue,sc);
        System.out.println("Binary Search Tree");
        printNode(rootValue);

    }
    public static void addChild(CreateNode node,Scanner sc) {
        if (node == null) {
            return;
        }
        System.out.println("Do you want to add the child for " + node.val + "Y/N");
        char choice = sc.next().charAt(0);
        if (choice == 'Y') {
            System.out.println("Enter the value for the child of " + node.val + ":");
            int childVal = sc.nextInt();
            addNode(node,childVal,sc);
            addChild(node,sc);

        }
//        System.out.println("Do you want to add more values? (Y/N)");
//        char moreValues = sc.next().charAt(0);
//        if (moreValues == 'Y') {
//            addChild(node, sc); // Recursively add more values
//        }
    }
    public static void addNode(CreateNode node, int val, Scanner sc) {
        if (node == null) {
            return;
        }

        if (val < node.val) {
            if (node.left == null) {
                node.left = new CreateNode(val);
            } else {
                addNode(node.left, val, sc);
            }
        } else {
            if (node.right == null) {
                node.right = new CreateNode(val);
            } else {
                addNode(node.right, val, sc);
            }
        }
    }
    public static void printNode(CreateNode node) {
        if (node == null) {
            return; // Exit the method if the node is null
        }
        System.out.println("Value: " + node.val);
        if (node.left != null) {
            System.out.println("Left child:");
            printNode(node.left); // Recursively print the left child
        }
        if (node.right != null) {
            System.out.println("Right child:");
            printNode(node.right); // Recursively print the right child
        }
    }

    }

