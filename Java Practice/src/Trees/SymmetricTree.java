package Trees;

import java.util.Scanner;

public class SymmetricTree {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the root value");
        int root = sc.nextInt();
        CreateNode rootValue = new CreateNode(root);

        addChild(rootValue,sc);
        boolean isSymmetric=isSymmetric(rootValue);
        System.out.println("The tree is Symmetric:"+ isSymmetric);

    }
    public static void addChild(CreateNode node , Scanner sc){
        System.out.println("Do you want to enter the left child for root value"+ node.val+"Y/N??");
        char choice=sc.next().charAt(0);
        if(choice=='Y'){
            System.out.println("Enter the left child value");
            int left=sc.nextInt();
            node.left=new CreateNode(left);
            addChild(node.left,sc);
        }
        System.out.println("Do you want to enter the right child for root value"+ node.val+"Y/N??");
        choice=sc.next().charAt(0);
        if(choice=='Y'){
            System.out.println("Enter the right child value");
            int right=sc.nextInt();
            node.right=new CreateNode(right);
            addChild(node.right,sc);
        }
    }
    public static boolean isSymmetric(CreateNode root) {
        if (root == null) {
            return true;
        }
        return isMirror(root.left, root.right);
    }
    public static boolean isMirror(CreateNode left,CreateNode right){
        if(left==null && right==null){
            return true;
        }
        if(left==null || right==null){
            return false;
        }
        if(left.val!= right.val) {
            return false;
        }
      return isMirror(left.left,right.right) && isMirror(left.right,right.left);
    }
}
