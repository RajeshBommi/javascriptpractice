package Trees;

import javax.swing.tree.TreeNode;
import java.util.Scanner;
import java.util.Stack;

public class InOrderUsingIterative {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the root value");
        int root = sc.nextInt();
        CreateNode rootValue = new CreateNode(root);

        addChild(rootValue, sc);

        System.out.println("InOrder Traversal");
        inOrder(rootValue);

    }
    public static void addChild(CreateNode node , Scanner sc) {
        System.out.println("Do you want to enter the left child for root value" + node.val + "Y/N??");
        char choice = sc.next().charAt(0);
        if (choice == 'Y') {
            System.out.println("Enter the left child value");
            int left = sc.nextInt();
            node.left = new CreateNode(left);
            addChild(node.left, sc);
        }
        System.out.println("Do you want to enter the right child for root value" + node.val + "Y/N??");
        choice = sc.next().charAt(0);
        if (choice == 'Y') {
            System.out.println("Enter the right child value");
            int right = sc.nextInt();
            node.right = new CreateNode(right);
            addChild(node.right, sc);
        }
    }

    public static void inOrder(CreateNode root){
        if(root==null){
            return;
        }
        Stack<CreateNode> s=new Stack<>();
        CreateNode current=root;

        while(current!=null || !s.isEmpty()) {
            while (current != null ) {
                s.push(current);
                current = current.left;
            }
               current = s.pop();
                System.out.println(current.val);
                current=current.right;

            }
        }
}
