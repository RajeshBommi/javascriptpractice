package Trees;

public class CreateNode {

    public static void main(String[] args) {

        CreateNode root = new CreateNode(12);
        root.left=new CreateNode(2);
        root.right=new CreateNode(3);

        CreateNode leftNode=root.left;
        CreateNode rightNode=root.right;

        leftNode.left=new CreateNode(4);
        rightNode.left=new CreateNode(5);
        rightNode.right=new CreateNode(6);

        System.out.println(root.val);

        System.out.println(root.right.val);

    }
    public int val;
    public CreateNode left;
    public CreateNode right;

    public CreateNode(int x) {
        val = x;
    }
}
