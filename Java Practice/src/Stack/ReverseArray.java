package Stack;

import java.util.Arrays;
import java.util.Stack;

public class ReverseArray {

    public static void main(String[] args) {

        int[]  arr = {1,2,3,4,5};

        Stack<Integer> stack1=new Stack<>();

        for(int i=0;i< arr.length;i++){
            stack1.push(arr[i]);
        }
//        for(int j=arr.length-1;j>=0;j--){
//            System.out.println(stack1.pop());
//        }

        int [] reversedArray = new int[arr.length];
        for(int j=0;j<arr.length;j++){
            reversedArray[j]=stack1.pop();
        }
        System.out.println(Arrays.toString(reversedArray));
    }
}
