package Strings;

import java.util.HashMap;
import java.util.Scanner;

public class IsomorphicStrings {

    public static void main(String[] args) {
        System.out.println(isomorphic("foo","bar"));
    }

    public  static boolean isomorphic(String s,String t){
        if(s.length()!=t.length()){
            return false;
        }
        HashMap<Character,Character> map= new HashMap<>();
        for(int i=0;i<s.length();i++) {
            char ori = s.charAt(i);
            char rep = t.charAt(i);


            if (!map.containsKey(ori)) {
                 if(!map.containsValue(rep)){
                     map.put(ori,rep);
                 }
                 else {
                     return false;
                 }
            }else{
                char mapped=map.get(ori);
                if(mapped!=rep){
                    return false;
                }
            }


        }
        return true;



    }
}
